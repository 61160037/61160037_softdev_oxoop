
public class Checkwin {

    static int sum;
    char turn;
    public static int End = 0;
    int row1;
    int col1;

    public void findTurn(int sum) {
        if (sum % 2 == 0) {
            turn = 'O';
        } else {
            turn = 'X';
        }
    }

    public int checkOxwin() {
        if (turn == 'O') {
             End = 1;
        } else {    
             End = 2;
        }
        return End;
    }

    public boolean checkwin(char[][] XO, int sum) {
        findTurn(sum);
        if (XO[row1][col1] == (turn) && XO[row1][col1 + 1] == (turn) && XO[row1][col1 + 2] == (turn)
                || XO[row1][col1] == (turn) && XO[row1 + 1][col1] == (turn) && XO[row1 + 2][col1] == (turn)
                || XO[row1][col1] == (turn) && XO[row1 + 1][col1 + 1] == (turn) && XO[row1 + 2][col1 + 2] == (turn)
                || XO[row1 + 1][col1] == (turn) && XO[row1 + 1][col1 + 1] == (turn) && XO[row1 + 1][col1 + 2] == (turn)
                || XO[row1 + 2][col1] == (turn) && XO[row1 + 2][col1 + 1] == (turn) && XO[row1 + 2][col1 + 2] == (turn)
                || XO[row1][col1 + 1] == (turn) && XO[row1 + 1][col1 + 1] == (turn) && XO[row1 + 2][col1 + 1] == (turn)
                || XO[row1][col1 + 2] == (turn) && XO[row1 + 1][col1 + 2] == (turn) && XO[row1 + 2][col1 + 2] == (turn)
                || XO[row1][col1 + 2] == (turn) && XO[row1 + 1][col1 + 1] == (turn) && XO[row1 + 2][col1] == (turn)) {
            checkOxwin();
            return true;
        }else {
            return false;
        } 
    }

    public int checkDraw() {
        if (sum == 8 && End == 0) {
            return End = 3;
        }
        return 0;
    }
}
