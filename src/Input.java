
import java.util.Scanner;

public class Input {
    Scanner kb = new Scanner(System.in);
    int row;
    int col;
    static int sum;
    public Input() {
    }
    public Input(int s) {
        sum = s;
    }
    public void setRowcol(char[][] XO,int sum) {
        System.out.print("Please input row,col : ");
        row = kb.nextInt() - 1;
        col = kb.nextInt() - 1;
        checkInputXo(XO,sum);
    }
    public void inputRowcol(char[][] XO,int sum) {
        System.out.print("Please input row,col : ");
        row = kb.nextInt() - 1;
        col = kb.nextInt() - 1;
        checkInputXo(XO,sum);
    }
    public void inputRow(char[][] XO,int sum) {
        System.out.print("Please input row : ");
        row = kb.nextInt() - 1;
        checkInputXo(XO,sum);
    }
    public void inputCol(char[][] XO,int sum) {
        System.out.print("Please input col : ");
        col = kb.nextInt() - 1;
        checkInputXo(XO,sum);
    }
    public void inputOx(char[][] XO,int sum) {
        if (sum % 2 == 0) {
            System.out.println("turn O");
            inputRowcol(XO,sum);
        } else {
            System.out.println("turn X");
            inputRowcol(XO,sum);
        }
    }
    public void addXo(char[][] XO,int sum) {
        if (sum % 2 == 0) {
            XO[row][col] = ('O');
        } else {
            XO[row][col] = ('X');
        }
    }
    public void checkInputXo(char[][] XO,int sum) {
        if ((row > 2 || row < 0) && col < 3) {
            System.out.println("Row : Please input num less than 3");
            inputRow(XO,sum);
        } else if ((col > 2 || col < 0) && row < 3) {
            System.out.println("Col : Please input num less than 3");
            inputCol(XO,sum);
        } else if (col > 2 && row > 2) {
            System.out.println("Row and Col : Please input num less than 3");
            inputOx(XO,sum);
        } else if (XO[row][col] == ('O') || XO[row][col] == ('X')) {
            System.out.println("NOT Empty!!");
            inputOx(XO,sum);
        } else {
            addXo(XO,sum);
        }
    }
}
