
public class Table {
    static char XO [][] = new char [3][3];
    
    public Table() {
        
    }
    public Table(char xo [][]) {
        XO = xo;
    }
    public void setTable(char[][] XO) {
        for (int j = 0; j < 3; j++) {
            for (int k = 0; k < 3; k++) {
                XO[j][k] = '-';
            }
        }
    }
    public void getTable(char[][] XO) {
        for (int h = 0; h < 3; h++) {
            for (int k = 0; k < 3; k++) {
                System.out.print(XO[h][k] + " ");
            }
            System.out.println();
        }
    }
}
