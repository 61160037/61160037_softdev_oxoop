
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        char[][] XO = new char[3][3];
        Table table = new Table(XO);
        Input input = new Input();
        Checkwin check = new Checkwin();
        Print print = new Print();
        int sum = 0;
        int End = check.checkOxwin();
        int playagain = 0;
        int Xwin = 0;
        int Owin = 0;
        int count = 0;

        while (playagain == 0) {
            System.out.println("Welcome to OX Game");
            table.setTable(XO);
            table.getTable(XO);
            while (true) {
                input.inputOx(XO, sum);
                table.getTable(XO);
                if (check.checkwin(XO, sum)) {
                    break;
                }
                check.checkDraw(sum ,End);
                sum++;
            }
            if (End == 1) {
                Owin++;
            } else if (End == 2) {
                Xwin++;
            }
            print.printXwin(End);
            print.printOwin(End);
            print.printDraw(End);

            count++;
            System.out.println("Play again ?");
            System.out.println("Yes hold 1 or No hold 2");
            playagain = kb.nextInt() - 1;
            if (Owin > Xwin) {
                System.out.println("O is Winner!! " + "\nScors O = " + Owin + " X = " + Xwin + " Draw = " + (count - (Xwin + Owin)));
            } else if (Owin < Xwin) {
                System.out.println("X is Winner!! " + "\nScors X = " + Xwin + " O = " + Owin + " Draw = " + (count - (Xwin + Owin)));
            } else {
                System.out.println("Draw!! " + "\n Scors O = " + Owin + " X = " + Xwin);
            }
        }
    }
}
